"use strict";

if (typeof Zig == 'undefined') {
    var Zig = {};
}
if (typeof Zig.Map == 'undefined') {
    Zig.Map = {};
}

(function(){
    Zig.Map.Mouse  = function(element) {
        this.element = null;
        this.pressed = false;
        this.pos  = {x : 0, y : 0};
        this.diff = {x : 0, y : 0};

        this._action = {};

        if (typeof element !== 'undefined') {
            this._watch(element);
        }
    };

    Zig.Map.Mouse.prototype = {
        getAction : function() {
            var action = this._action;
            this._action = {};

            return action;
        },

        _watch: function(element) {
            var self = this;
            this._element = element;
            window.addEventListener('mousemove', this._move.bind(this), true);
            this._element.addEventListener('mousedown', this._down.bind(this), true);
            window.addEventListener('mouseup', this._up.bind(this), true);

            this._element.addEventListener('selectstart', function(e) {
                e.preventDefault();
            }, true);
        },

        _move: function(e) {
            var x = e.offsetX || e.layerX,
                y = e.offsetY || e.layerY;

            this.diff.x += Math.abs(this.pos.x - x);
            this.diff.y += Math.abs(this.pos.y - y);

            if (this.pressed) {
                this._addToAction('drag', this.pos.x - x, this.pos.y - y);
            } else {
                this._action.move = {x : x, y : y};
            }

            this.pos.x = x;
            this.pos.y = y;
        },

        _down: function(e) {
            this.pos = {x : e.offsetX || e.layerX, y : e.offsetY || e.layerY};
            this.diff = {x : 0, y : 0};
            this.pressed = true;
            this.click   = true;

            this._showMoveCursor(true);
            this._action.down = {x : this.pos.x, y : this.pos.y};
        },

        _up: function(e) {
            if (this.pressed) {
                this.pressed = false;

                this._showMoveCursor(false);
                this._action.up = {x : this.pos.x, y : this.pos.y};
                if (this.diff.x <= 1 && this.diff.y <= 1) {
                    this._action.click = {x : this.pos.x, y : this.pos.y};
                }
            }
        },

        _addToAction : function(key, x, y) {
            if (typeof this._action[key] == 'undefined') {
                this._action[key] = {x : 0, y : 0};
            }

            this._action[key].x += x;
            this._action[key].y += y;
        },

        _showMoveCursor : function (b) {
            if (b) {
                if (Zig.Utils.browser == 'mozilla') {
                    document.body.style.cursor = '-moz-grabbing !important';
                } else if (Zig.Utils.browser == 'msie') {
                    document.body.style.cursor = 'url(img/cursor/closedhand.cur), move';
                } else {
                    document.body.style.cursor = 'url(img/cursor/closedhand.cur) 4 4, move';
                }
            } else {
                document.body.style.cursor = 'default';
            }
        },
    };
})();