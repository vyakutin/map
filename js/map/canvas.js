"use strict";

if (typeof Zig == 'undefined') {
    var Zig = {};
}
if (typeof Zig.Map == 'undefined') {
    Zig.Map = {};
}

(function(){
    Zig.Map.Canvas = function(width, height, core) {
        Zig.EventEmitter.call(this);

        this.width  = width;
        this.height = height;
        this.core   = core;
        this.canvas = [];

        var list = [
            {width : width, height : height},
            {width : width * 2, height : height * 2},
            {width : width * 2, height : height * 2},
            {width : width * 2, height : height * 2}
        ];

        list.forEach(function(v) {
            var tmp = this._createCanvas(v.width, v.height);
            if (!tmp) { return; }

            this.canvas.push(tmp);
        }.bind(this));

        if (this.canvas.length != list.length) {
            throw Zig.Error.NoSupportCanvas;
        }

        $('.map-container').append(this.canvas[0].canvas);
        this._mouse = new Zig.Map.Mouse(this.canvas[0].canvas);
    };

    Zig.Map.Canvas.prototype = {
        render : function() {
            requestAnimationFrame(this.render.bind(this));

            var pos = this.core.render(this.canvas[1].ctx, this.canvas[2].ctx, this._mouse);

            this.canvas[0].ctx.clearRect(0, 0, this.width, this.height);
            this.canvas[0].ctx.drawImage(this.canvas[1].canvas, -pos.x, -pos.y);
            this.canvas[0].ctx.drawImage(this.canvas[2].canvas, -pos.x, -pos.y);
        },

        clearBuffer : function(){
            this.canvas[1].ctx.clearRect(0, 0, this.width * 2, this.height * 2);
        },

        clearBuffer2 : function() {
            this.canvas[2].ctx.clearRect(0, 0, this.width * 2, this.height * 2);
        },

        _createCanvas : function(width, height) {
            var canvas = document.createElement('canvas');
            canvas.width  = width;
            canvas.height = height;

            if (!canvas.getContext('2d')) {
                document.body.innerHTML = '<div style="text-align: center;">No support 2d context.</div>';
                return false;
            }

            var ctx = canvas.getContext('2d');
            ctx.font = '16px monospace';
            ctx.textAlign = 'center';

            return {canvas : canvas, ctx : ctx};
        }
    };

    Zig.Utils.inherit(Zig.Map.Canvas, Zig.EventEmitter);
})();