"use strict";

if (typeof Zig == 'undefined') {
    var Zig = {};
}
if (typeof Zig.Map == 'undefined') {
    Zig.Map = {};
}

(function(){
    Zig.Map.Cell = function(image, x, y, cell) {
        this.image = image;
        this.x = x;
        this.y = y;
        this.width  = cell.width;
        this.height = cell.height;

        this.pos = {
            x : x * cell.width,
            y : y * cell.height
        };
    };

    Zig.Map.Cell.prototype = {
        draw : function(buffer, x, y) {
            var img = Zig.Map.Resources.get(this.image);

            if (img) {
                buffer.drawImage(img, this.pos.x - x, this.pos.y - y);
            }
        },

        getPos : function() {
            return this.pos;
        },

        focus : function(ctx, x, y) {
            ctx.beginPath();
            ctx.arc(this.pos.x - x - this.width / 2, this.pos.y - y - this.height / 2, this.width / 2, 0, 2 * Math.PI, false);
            ctx.lineWidth = 2;
            ctx.strokeStyle = 'red';
            ctx.stroke();
        },

        select : function(ctx, x, y) {
            ctx.beginPath();
            ctx.arc(this.pos.x - x - this.width / 2, this.pos.y - y - this.height / 2, this.width / 2, 0, 2 * Math.PI, false);
            ctx.lineWidth = 2;
            ctx.strokeStyle = 'blue';
            ctx.stroke();
        },
    };
})();